import {combineReducers} from "redux";
import quizReduces from "./quizReducer";
import createReducer from "./createReducer";
import authReducer from "./authReducer";

export default combineReducers({
    quiz: quizReduces,
    create: createReducer,
    auth: authReducer
})