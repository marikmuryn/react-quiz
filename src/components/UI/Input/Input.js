import React from "react";
import classes from './Input.css'

const isInValid = (valid, touched, shouldValidate) => (
    !valid && shouldValidate && touched
)

const Input = ({type, label, value, onChange, errorMessage, valid, touched, shouldValidate}) => {
    const cls = [classes.Input]
    const inputType = type || 'text'

    if (isInValid(valid, touched, shouldValidate)) {
        cls.push(classes.invalid)
    }

    return (
       <div className={cls.join(' ')}>
           <label>
               <span>{label}</span>
               <input
                   type={inputType}
                   value={value}
                   onChange={onChange}
               />
           </label>
           {isInValid(valid, touched, shouldValidate)
               ? <span>{errorMessage || 'Введіть правильно дані'}</span>
               : null
           }
       </div>
    )
}

export default Input
