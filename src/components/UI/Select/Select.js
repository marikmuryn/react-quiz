import React from "react";
import classes from './Select.css'

const Select = ({value, label, options, onChange}) => (
    <div className={classes.Select}>
        <label>
            {label}
            <select
                value={value}
                onChange={onChange}
            >
                {
                    options.map((option, index) => {
                        return (
                            <option
                                key={option.value + index}
                                value={option.value}
                            >
                                {option.text}
                            </option>
                        )
                    })
                }
            </select>
        </label>
    </div>
)

export default Select
