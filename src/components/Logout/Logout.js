import React, {Component} from "react";
import {connect} from 'react-redux'
import {Redirect} from 'react-router-dom'
import {logout} from "../../store/action/authAction";

class Logout extends Component {
    componentDidMount() {
        this.props.logout()
    }

    render() {
        return (
            <Redirect to={'/'}>Вийти</Redirect>
        )
    }
}

const mapDispatchToProps = dispatch => ({logout: () => dispatch(logout())})

export default connect(null, mapDispatchToProps)(Logout)
