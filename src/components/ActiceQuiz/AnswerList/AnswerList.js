import React from "react";
import classes from './AnswerList.css'
import AnswerItem from "./AnswerItem/AnswerItem";

const AnswerList = ({answers, onAnswerClick, state}) =>(
    <ul className={classes.AnswerList}>
        {answers.map((answer, index) =>
            <AnswerItem
                key={index}
                answer={answer}
                onAnswerClick={onAnswerClick}
                state={state ? state[answer.id] : null}
            />
        )}
    </ul>
)

export default AnswerList
