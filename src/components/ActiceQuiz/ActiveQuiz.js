import React from "react";
import classes from './ActiveQuiz.css'
import AnswerList from "./AnswerList/AnswerList";

const ActiveQuiz = ({question, answers, onAnswerClick, answerNumber, quizLength, state}) => (
    <div className={classes.ActiveQuiz}>
        <p className={classes.Question}>
            <span>
                <strong>1. </strong>
                {question}
            </span>

            <small>{answerNumber}/{quizLength}</small>
        </p>
        <AnswerList
            answers={answers}
            state={state}
            onAnswerClick={onAnswerClick}
        />
    </div>
)

export default ActiveQuiz
