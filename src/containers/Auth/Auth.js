import React, {Component} from "react";
import {connect} from 'react-redux'
import classes from './Auth.css'
import Button from "../../components/UI/Button/Button";
import Input from "../../components/UI/Input/Input";
import is from 'is_js'
import {auth} from "../../store/action/authAction";

class Auth extends Component {
    state = {
        isFormValid: false,
        formControls: {
            email: {
                type: 'email',
                value: '',
                label: 'Email',
                errorMessage: 'Введіть правильний email',
                valid: false,
                touched: false,
                validation: {
                    require: true,
                    email: true
                }
            },
            password: {
                type: 'password',
                value: '',
                label: 'Password',
                errorMessage: 'Введіть правильний пароль',
                valid: false,
                touched: false,
                validation: {
                    require: true,
                    minLength: 6
                }}
        }
    }

    loginHandler = () => {
        this.props.auth(
            this.state.formControls.email.value,
            this.state.formControls.password.value,
            true
        )
    }

    registerHandler = () => {
        this.props.auth(
            this.state.formControls.email.value,
            this.state.formControls.password.value,
            false
        )
    }

    submitHandler = (event) => {
        event.preventDefault()
    }

    validateControl = (value, validation) => {
        if(!validation) {
            return true
        }

        let isValid = true

        if(validation.require) {
            isValid = value.trim() !== '' && isValid
        }

        if(validation.email) {
            isValid = is.email(value) && isValid
        }

        if(validation.minLength) {
            isValid = value.length >= validation.minLength && isValid
        }

        return isValid
    }

    onChangeHandler = (event, controlName) => {
        const formControls = {...this.state.formControls}
        const control = {...formControls[controlName]}

        control.value = event.target.value;
        control.touched = true
        control.valid = this.validateControl(control.value, control.validation)

        formControls[controlName] = control

        let isFormValid = true

        Object.keys(formControls).forEach(name => {
            isFormValid = formControls[name].valid && isFormValid
        })

        this.setState({
            formControls, isFormValid
        })
    }

    renderInputs = () => {
        return Object.keys(this.state.formControls).map((controlName, index) => {
            const {type, value, label, errorMessage, valid, touched, validation} = this.state.formControls[controlName]
            return (
                <Input
                    key={controlName + index}
                    type={type}
                    value={value}
                    label={label}
                    errorMessage={errorMessage}
                    valid={valid}
                    touched={touched}
                    shouldValidate={!!validation}
                    onChange={(event) => this.onChangeHandler(event, controlName)}
                />
            )
        })
    }

    render() {
        return (
            <div className={classes.Auth}>
                <h1>Авторизація</h1>
                <form onSubmit={this.submitHandler} className={classes.AuthForm}>
                    {this.renderInputs()}
                    <Button
                        type="success"
                        onClick={this.loginHandler}
                        disabled={!this.state.isFormValid}
                    >Увійти</Button>
                    <Button
                        type="primary"
                        onClick={this.registerHandler}
                        disabled={!this.state.isFormValid}
                    >Зареєструватися</Button>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        auth: (email, password, isLogin) => dispatch(auth(email, password, isLogin))
    }
}

export default connect(null, mapDispatchToProps)(Auth)