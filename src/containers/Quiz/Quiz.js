import React, {Component} from "react";
import {connect} from 'react-redux'
import classes from './Quiz.css'
import ActiveQuiz from "../../components/ActiceQuiz/ActiveQuiz";
import FinishedQuiz from "../../components/FinishedQuiz/FinishedQuiz";
import Loader from "../../components/UI/Loader/Loader";
import {fetchQuizById, quizAnswerClick, retryQuiz} from "../../store/action/quizAction";

class Quiz extends Component {
    componentDidMount() {
        this.props.fetchQuizById(this.props.match.params.id)
    }

    componentWillUnmount() {
        this.props.retryQuiz()
    }

    render() {
        return (
            <div className={classes.Quiz}>
                <h1>Quiz</h1>
                {this.props.loading || !this.props.quiz
                    ? <Loader/>
                    : this.props.isFinished
                        ? <FinishedQuiz
                            results={this.props.results}
                            quiz={this.props.quiz}
                            onRetry={this.props.retryQuiz}
                        />
                        :
                        <div className={classes.QuizWrapper}>
                            <ActiveQuiz
                                answers={this.props.quiz[this.props.activeQuestion].answers}
                                question={this.props.quiz[this.props.activeQuestion].question}
                                onAnswerClick={this.props.quizAnswerClick}
                                quizLength={this.props.quiz.length}
                                answerNumber={this.props.activeQuestion + 1}
                                state={this.props.answerState}
                            />
                        </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    results: state.quiz.results,
    isFinished: state.quiz.isFinished,
    activeQuestion: state.quiz.activeQuestion,
    answerState: state.quiz.answerState,
    loading: state.quiz.loading,
    quiz: state.quiz.quiz,
})

const mapDispatchToProps = dispatch => ({
    fetchQuizById: id => dispatch(fetchQuizById(id)),
    quizAnswerClick: answerId => dispatch(quizAnswerClick(answerId)),
    retryQuiz: () => dispatch(retryQuiz())
})

export default connect(mapStateToProps, mapDispatchToProps)(Quiz)
