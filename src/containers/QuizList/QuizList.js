import React, {Component} from "react";
import {NavLink} from "react-router-dom";
import classes from './QuizList.css'
import Loader from "../../components/UI/Loader/Loader";
import {connect} from 'react-redux'
import {fetchQuizes} from "../../store/action/quizAction";

class QuizList extends Component {
    renderQuizes = () => {
        return this.props.quizes.map(quiz => (
            <li key={quiz.id}>
                <NavLink to={'/quiz/' + quiz.id}>
                    {quiz.name}
                </NavLink>
            </li>
        ))
    }

    componentDidMount() {
        this.props.fecthQuizes()
    }

    checkEmptyList() {
        return this.props.quizes.length === 0 ? 'На даний момент тестів немає' : <ul> {this.renderQuizes()} </ul>
    }

    render() {
        return (
            <div className={classes.QuizList}>
                <div>
                    <h1>Список тестів</h1>
                    {this.props.loading && this.props.quizes.length !== 0
                        ? <Loader/>
                        : this.checkEmptyList()
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    quizes: state.quiz.quizes,
    loading: state.quiz.loading
})

const mapDispatchToProps = dispatch => ({fecthQuizes: () => dispatch(fetchQuizes())})

export default connect(mapStateToProps, mapDispatchToProps)(QuizList)