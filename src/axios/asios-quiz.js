import axios from 'axios'

export default axios.create({
    baseURL: 'https://react-quiz-e0e8c.firebaseio.com/'
})